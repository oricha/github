# GithubTopUser
----------------------

You must run the application on a pc with github configured, /<User>/.github created
Add oauthAccessToken and user.name on aplication.properties


```properties
oauthAccessToken = yada, yada, yada...
user.name = yada, yada, yada..
```


*Run*

 -  mvn package
 
 - mvn test
 
 -  mvn spring-boot:run


*REST Endpoint*

 [http://localhost:8080/search/{city}?top=5](http://localhost:8080/search/Barcelona?top=5) 

● Given a city name (e.g. Barcelona) the service returns a list of the top contributors (by
number of repositories) in GitHub.
