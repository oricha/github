package com.example;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.kohsuke.github.GHUser;
import org.kohsuke.github.GitHub;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class WebControllerTests {
	
	  @LocalServerPort
	    private int port;

	    private URL base;

	    @Autowired
	    private TestRestTemplate restTemplate;
	    
	    @Value("${user.name}")
		private String login;

		@Value("${oauthAccessToken}")
		private String oauthAccessToken;
		
		String CITY = "Barcelona";
	    
	    @Before
	    public void setUp() throws Exception {
	        this.base = new URL("http://localhost:" + port + "/");
	    }
	    @Ignore
	    @Test
	    public void indexTest() throws Exception {
	        ResponseEntity<String> response = restTemplate.getForEntity(base.toString(),
	                String.class);
	        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
	    }
	    @Test
	    public void top5Test() throws Exception {
	    	Map<String, String> params = new HashMap<String, String>();
			params.put("top", "5");
			

			@SuppressWarnings("rawtypes")
			ResponseEntity<List> entity = this.restTemplate.getForEntity(base +"/search/" + CITY+"?top={top}",
					List.class, params);
			assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.OK);
	    	
	    }
	    @Ignore
	    @Test
	    public void top10Test() throws Exception {
	    	Map<String, String> params = new HashMap<String, String>();
			params.put("top", "10");
			String city = "Barcelona";

			@SuppressWarnings("rawtypes")
			ResponseEntity<List> entity = this.restTemplate.getForEntity(base +"/search/" + CITY+"?top={top}",
					List.class, params);
			assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.OK);
	    	
	    }
	    @Ignore
	    @Test
	    public void top50Test() throws Exception {
	    	Map<String, String> params = new HashMap<String, String>();
			params.put("top", "50");
			String city = "Barcelona";

			@SuppressWarnings("rawtypes")
			ResponseEntity<List> entity = this.restTemplate.getForEntity(base +"/search/" + CITY+"?top={top}",
					List.class, params);
			assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.OK);
	    	
	    }
//	    @Test
//	    public void test() throws IOException{
//	    	GitHub gh = GitHub.connect(login, oauthAccessToken);
//	    	for (GHUser u : gh.searchUsers().location("Barcelona").list()) {
//	    		System.out.println("name: " + u.getName() + " repos: " + u.getRepositories().size());
//	    	}
//	    }
}
