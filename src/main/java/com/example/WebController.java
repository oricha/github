package com.example;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import javax.ws.rs.Produces;

import org.kohsuke.github.GHUser;
import org.kohsuke.github.GitHub;
//import org.kohsuke.github.GHUser;
//import org.kohsuke.github.GitHub;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@Controller
@RestController
public class WebController {

	@Value("${user.name}")
	private String login;

	@Value("${oauthAccessToken}")
	private String oauthAccessToken;

	@RequestMapping("/")
	public String index() {
		return "Greetings from Spring Boot!";
	}

	@RequestMapping(value = "/search/{city}", method = RequestMethod.GET)
	@Produces("application/json")
	public @ResponseBody List<GHUser> search(@PathVariable String city,
			@RequestParam(required = false, defaultValue = "5") Integer top) throws IOException {
		LinkedList<GHUser> usersList = new LinkedList<GHUser>();

		GitHub gh = GitHub.connect(login, oauthAccessToken);
		int count = 0;
		
//		GET /search?q=repos%3A1+location%3Barcelona&type=Users
		for (GHUser u : gh.searchUsers().location(city).list()) {
			usersList.add(u);
			if (count < 150) {
				System.out.print(".");
				count++;
			} else {
				System.out.println(".");
				count = 0;
			}
		}
//		System.out.println("sorting ");
		Collections.sort(usersList, new Comparator<GHUser>() {
			@Override
			public int compare(GHUser u1, GHUser u2) {
				try {
//					GET /users/:user/repos
					if (u1.getRepositories().size() > u2.getRepositories().size())
						return 1;
					else if (u1.getRepositories().size() < u2.getRepositories().size())
						return -1;
				} catch (IOException e) {
					e.printStackTrace();
				}
				return 0;
			}
		});
		System.out.println("sorted ");
		
		return new ArrayList<GHUser>(usersList.subList(0, top-1));
	}

}
